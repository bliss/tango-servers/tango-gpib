#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <cstring>
#include "gpibAccess.h"

#include "ib.h"


using namespace std;

/**
 * Standard GPIB errors strings.
 * When the MSB bit (= bit 15) is set in ibsta, then
 * iberr contains one of the following errors.
 * When iberr contains EDVR, then the system error is in ibcnt.
 */
const string error_array[] = {
        "EDVR - Unix system error: ",    // code is in ibcnt
        "ECIC - Function require GPIB board to be Controller-In-Charge",
        "ENOL - Write handshake error (e.g. no listener)",
        "EADR - GPIB board not addressed correctly",
        "EARG - Invalid argument to function call",
        "ESAC - GPIB board is not Active System Controller as required",
        "EABO - Asynchronous I/O operation was aborted (timeout)",
        "ENEB - Non-existent GPIB board",
        "EDMA - DMA hardware problem",
        "???? - UNKNOWN ERROR !",   // index 9 
        "EOIP - New I/O attempted with old I/O in progress",
        "ECAP - No capability for intended operation",
        "EFSO - File system error",
        "???? - UNKNOWN ERROR !",   // index 13
        "EBUS - GPIB bus error",
        "ESTB - Serial poll status-byte-queue overflow",
        "ESRQ - SRQ stuck in ON position",
        "???? - UNKNOWN ERROR !",   // index 17
        "???? - UNKNOWN ERROR !",   // index 18
        "???? - UNKNOWN ERROR !",   // index 19
        "ETAB - Table problem (The return buffer is full)"
};


//------------------ padAndSadToString() -------------------
/**
 * This auxiliary function, which is defined outside gpibDevice class,
 * combines Primary and Secondary addresses to form a string of format
 * "pad_sad" in order to use is as a dev_name in gpibDeviceException()
 * invocation or in some debug printing.
 * Need such a function, since if other than the 1st or 2nd constructor
 * is used to instantiate a gpibDevice object (which will be the case on
 * beamlines, where we will use 3rd->6th constructor to instantiate a device),
 * the dev_name is an empty string. We do not use any more programs like
 * ibconf to create device's (logical) name.
 * This function needs the following C++ header functions:
 * <string> and <sstream>
 *
 * Usage: Internally in 3rd, 4th, 5th and 6th constructor.
 *        TODO: In device server it will be used at least in init_device().
 */
string padAndSadToString(int pad, int sad)
{
    string r;
    stringstream s;

    s << pad;
    s << "_";
    s << sad;
    r = s.str();

    return r;
}

//+----------------------------------------------------------------------------
//
//      Class gpibDevice Implementation:
//
//-----------------------------------------------------------------------------

//
// PUBLIC METHODS of class gpibDevice()
// ------------------------------------
//

// 'open'/'close' - related public functions:
// ------------------------------------------
// In constructors 1->2 use ibfind() to 'open' board or device
//                 3->6 use ibdev()  to 'open' device.
// In setOffLine() we use ibonl() to 'close' board or device.

//------------------ gpibDevice::gpibDevice() 1st -------------------
/**
 * This is the 1st constructor for the gpibDevice class.
 * The parameters passed to this constructor are:
 * - the device name as defined in the GPIB driver with the "ibconf" program,and
 * - the board name, where the device is connected to. The boardname is in a
 *   form 'gpibN', where N is a digit starting with 0.
 *
 * It opens a device by ibfind() call using device's name.
 *
 * IMPORTANT NOTES:
 * All devices plugged in a board different from gpib0 and which are to be
 * found and instantiated on the basis of their name (and not on the basis
 * of their primary + secondary addresses as is the case in 3rd->6th
 * constructors further below), MUST use this constructor.
 * If device is on board gpib0, it can use this 1st constructor or even better
 * the 2nd one. So instantiation with gpibDevice("dev3","gpib0") [= this 1st
 * constructor] is equivalent to gpibDevice("dev3") [= with 2nd constructor].
 *
 * On NI Web site it is recommended that ibfind() (used in this and the 2nd
 * constructor) is used only for opening path to the GPIB controller board
 * and not to be used to open the path to a GPIB device. For opening path
 * to a device, NI recommends to use rather ibdev() (used in the last 4
 * constructors). On beamline we will respect this convention --> see comment
 * that follows.
 *
 * On beamlines since we do not use logical names for GPIB device and will
 * use primary address (+ maybe secondary addr.) to open the path to a GPIB
 * device (--> therefore using the last 4 constructors) and since in
 * instantiating a board class the 2nd constructor is used, this one (= the 1st
 * constructor) will never be used, but we leave it here for completeness and
 * in case it is used in the machine control system. 
 *
 * If a method doesn't throw exception, this does not necessarily mean that the
 * GPIB device exists. This means that the name devX exists in the GPIB driver!
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server used to instantiate a device (when device is 
 *        identified with its logical name and a board name).
 */
gpibDevice::gpibDevice(string devname, string boardname)
{
    int    devhandle, pad, sad, boardindex;
    string ss;

    is_a_board   = false;
    debug_flag = false;

    cout << "1st gpibDevice constr: Enter " << endl;
    cout << "1st gpibDevice constr: GPIB Device name = " << devname << endl;
    cout << "1st gpibDevice constr: GPIB Board  name = " << boardname << endl;

    // Extract board index from board name
    resetState();
    ss = boardname.substr(4);
    boardindex = atoi( ss.c_str() );

    if ( (boardindex < DEFAULT_BOARD_INDEX) || (boardindex > MAX_BOARD_INDEX) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
             ": Board index " << boardindex << " is out of range [0,7]" << endl;
        throw gpibDeviceException( devname, \
            "Error occured while getting board index ", \
            "Board index is out of range.", \
            "Value must be between 0 and 7", getiberr(),getibsta());
    }

    // Get Device by name; return device handle (devhandle)
    resetState();
    devhandle = ibfind((char *) devname.c_str() );
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
                ": An error occured executing ibfind()."<< endl;
        cout << "Error code = " << getiberr()<<"("<< iberrToString()<<")"<<endl;
        cout << "Ibsta code = " << getibsta()<<"("<< ibstaToString()<<")"<<endl;
        throw gpibDeviceException( devname, \
            "Error occured executing ibfind() to open GPIB device", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    // Ask for device's primary address
    resetState();
    ibask(devhandle,IbaPAD,&pad); // Get Device's Primary Address
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
        ": An error occured executing ibask() to get primary address."<< endl;
        cout << "Error code = " << getiberr()<<"("<< iberrToString()<<")"<<endl;
        cout << "Ibsta code = " << getibsta()<<"("<< ibstaToString()<<")"<<endl;
        throw gpibDeviceException( devname, \
            "Error occured in asking for device Primary Address ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    // Ask for device's secondary address
    resetState();
    ibask(devhandle,IbaSAD,&sad); // Get Device' Secondary Address
    // Remark: If secondary address obtained is 0, this means that this device
    //         is not using it (= sec. address is disabled).
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
        ": An error occured executing ibask() to get secondary address."<< endl;
        cout << "Error code = " << getiberr()<<"("<< iberrToString()<<")"<<endl;
        cout << "Ibsta code = " << getibsta()<<"("<< ibstaToString()<<")"<<endl;
        throw gpibDeviceException( devname, \
            "Error occured in asking for device Secondary Address ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    // Now that all is OK, copy to internal gpibDevice class fields.
    dev_name    = devname;
    dev_handle  = devhandle;
    dev_pad     = pad;
    dev_sad     = sad;
    board_index = boardindex;

    // Remark: Having device handle (dev_handle) can obtain board index
    //         also with a call:
    //         ibask(dev_handle,IbaBNA,&board_index);
    //         and if all OK, should get the same number as extracted from
    //         the argin boardname in the beginning of this function.

    cout << "1st gpibDevice constr: Leave " << endl;
};


//------------------ gpibDevice::gpibDevice() 2nd -------------------
/**
 * This is the 2nd constructor for the gpibDevice class.
 * The parameter passed to this constructor is:
 * - the device name as defined in the GPIB driver with the "ibconf" program
 *   when gpibDevice object is instantiated on the basis of device's name or
 * - the board name when gpibBoard (= derived class) object is instantiated.
 * If parameter is a device name then the board is 'gpib0' and
 * if parameter is a board name it must be in a form 'gpibN', 
 * where N is a digit starting with 0 and represents board index.
 *
 * IMPORTANT NOTES:
 * When this constructor is used to instantiate a device, the board name is
 * not passed to it and therefore this constructor assumes that this GPIB
 * device is connected to the board with index 0 (i.e. that the board name
 * is "gpib0")!
 * So gpibDevice("dev3","gpib0") [using the 1st constructor] is equivalent
 * to gpibDevice("dev3")         [using the 2nd constructor]
 * It opens a device by ibfind() call using device's name
 *
 * When this constructor is used to instantiate gpibBoard() as a subclass
 * of gpibDevice() super-class, then the parameter passed to this constructor
 * is the boardname. In this case first the body of super-class (= gpibDevice())
 * constructor is executed and if no exception is thrown then the body of the
 * gpibBoard() constructor is executed, which sets the board_index to the
 * correct value.
 *
 * So this is THE ONLY gpibDevice() constructor among all 6 which can be used
 * to instantiate object for GPIB device OR GPIB board. All other 5 gpibDevice() 
 * constructors can be used only for instantiating GPIB device objects.
 * 
 * As mentioned in the header of the 1st constructor, we will use this 2nd
 * constructor only to instantiate a board object and on beamlines will use 
 * the last 4 constructors to instantiate a device object (so we respect the NI
 * recommended usage of ibfind() and ibdev()).
 *
 * If a method doesn't throw exception, this does not necessarly mean that the
 * GPIB device exists. This means that the name devX exists in the GPIB driver!
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server used to instantiate a device (when device is 
 *        identified with its logical name) OR a board (when a board is
 *        is identified with its name).
 */
gpibDevice::gpibDevice(string devname)
{
    int devhandle, pad, sad, boardindex;

    is_a_board   = false;
    debug_flag = false;

    cout << "2nd gpibDevice constr: Enter " << endl;
    cout << "2nd gpibDevice constr: GPIB Device/Board name = " << \
            devname << endl;

    // Analyze the name to know whether is the device (invoked in case of
    // a GPIB device) or board (when invoked as super-class of the gpibBoard
    // class). We know that the board name is "gpibN" where N is small integer
    // starting at 0. We do analysis of the name in order to put out right
    // comments with cout and with gpibDeviceException().
    resetState();
    size_t gpib_pos = devname.find("gpib");
    if (gpib_pos != string::npos)
    {
        // found substring gpib in the devname --> very likely we have
        // the case when gpibDevice() constructor was invoked because
        // gpibDevice class is a super-class of gpibBoard class.
        is_a_board = true;
    }
    cout << "2nd gpibDevice constr: is_a_board = " << is_a_board << endl;

    if (is_a_board == true)
    {
        cout << "2nd constr: GPIB Board name = " << devname << endl;
        // Extract board index from board name
        string ss;
        ss = devname.substr(4);
        boardindex = atoi( ss.c_str() );
        if( (boardindex < DEFAULT_BOARD_INDEX)||(boardindex > MAX_BOARD_INDEX) )
        {
            cout << "Board = " << devname << ": Board index " << boardindex <<\
                    " is out of range [0,7]" << endl;
            throw gpibDeviceException( devname, \
                "Error occured while getting board index ", \
                "Board index is out of range.", \
                "Value must be between 0 and 7", getiberr(),getibsta());
        }
    }
    else
    {
        cout << "2nd constr: GPIB Device name (must be on board 0) = " \
                 << devname << endl;
        boardindex = DEFAULT_BOARD_INDEX;
    }

    resetState();
    // Get Device/board by name; return device/board handle (devhandle)
    devhandle = ibfind((char *) devname.c_str() );
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        if (is_a_board == true)
        {
            cout << "Board " << devname << \
            ": An error occured while executing ibfind()."<< endl;
        }
        else
        {
            cout << "Board:Device = " << "gpib0:" << devname << \
            ": An error occured while executing ibfind()."<< endl;
        }
        cout << "Error code = " << getiberr()<<"("<< iberrToString()<<")"<<endl;
        cout << "Ibsta code = " << getibsta()<<"("<< ibstaToString()<<")"<<endl;
        throw gpibDeviceException( devname, \
            "Error occured while executing ibfind() ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    resetState();
    ibask(devhandle,IbaPAD,&pad); // Get Primary Address (valid for device
                                  //                            and board)
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        if (is_a_board == true)
        {
            cout << "Board " << devname << \
          ": An error occured executing ibask() to get primary address."<< endl;
        }
        else
        {
            cout << "Board:Device = " << "gpib0:" << devname << \
          ": An error occured executing ibask() to get primary address."<< endl;
        }
        cout << "Error code = " << getiberr()<<"("<< iberrToString()<<")"<<endl;
        cout << "Ibsta code = " << getibsta()<<"("<< ibstaToString()<<")"<<endl;
        throw gpibDeviceException( devname, \
            "Error occured in asking for Primary Address ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    resetState();
    ibask(devhandle,IbaSAD,&sad); // Get Secondary Address (valid for device
                                  //                              and board)
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        if (is_a_board == true)
        {
            cout << "Board " << devname << \
        ": An error occured executing ibask() to get secondary address."<< endl;
        }
        else
        {
            cout << "Board:Device = " << "gpib0:" << devname << \
        ": An error occured executing ibask() to get secondary address."<< endl;
        }
        cout << "Error code = " << getiberr()<<"("<< iberrToString()<<")"<<endl;
        cout << "Ibsta code = " << getibsta()<<"("<< ibstaToString()<<")"<<endl;
        throw gpibDeviceException( devname, \
            "Error occured in asking for Secondary Address ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta());
    }

    // Now that all is OK, copy to internal gpibDevice class fields.
    dev_name    = devname;  // is boardname in case of a board
    dev_handle  = devhandle;
    dev_pad     = pad;
    dev_sad     = sad;
    board_index = boardindex;

    // When devname on input is the board name (i.e. when this class is
    // instantiated in gpibBoard (= derived) calss), then board_index
    // is overwritten in the derived class.

    cout << "2nd gpibDevice constr: Leave " << endl;
};


//------------------ gpibDevice::gpibDevice() 3rd -------------------
/**
 * This is the 3rd constructor for the gpibDevice class.
 * The parameters passed to this constructor are:
 * - the device primary address, which should be one of [1,30].
 * - the board name, where the device is connected to. The boardname is in a
 *   form 'gpibN', where N is a digit starting with 0.
 *
 * It opens a device by ibdev() call using device's primary address and
 * a board name.
 * This constructor assumes your GPIB device secondary address (the 3rd
 * argument to ibdev()) to be NO_SAD = 0, i.e. the secondary address is
 * disabled/unused!
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server used to instantiate a device (when device is 
 *        identified with its primary and a board name).
 */
gpibDevice::gpibDevice(int primary_add, string boardname)
{
    int    devhandle, pad, sad, boardindex;
    string devname;
    string ss;

    is_a_board   = false;
    debug_flag = false;

    cout << "3rd gpibDevice constr: Enter " << endl;
    cout << "3rd gpibDevice constr: GPIB Device Primary address = " << \
             primary_add << endl;
    cout << "3rd gpibDevice constr: GPIB Board  name = " << boardname << endl;

    resetState();
    if ( (primary_add < 1) || (primary_add > gpib_addr_max) )
    {
        cout << "Primary address " << primary_add << \
                " has bad value; should be [1,30]" << endl;
        dev_name = "not-yet-defined";
        throw gpibDeviceException( dev_name, \
        "Error found in checking primary address ", \
        "Primary address is out of range.", \
        "Value must be between 1 and 30", getiberr(), getibsta());
    }

    // Extract board id (= board index).
    ss = boardname.substr(4);
    boardindex = atoi( ss.c_str() );

    // Since here do not have a meaningfull dev_name (it is an empty
    // string), compose it from device's primary and secondary addresses.
    // in order to use it in gpibDeviceException().
    // This allow us to leave gpibDeviceException.h and .cpp intact.
    // Use 0 for secondary address = sec. address disabled/not used.
    devname = padAndSadToString(primary_add, NO_SAD);

    if ( (boardindex < DEFAULT_BOARD_INDEX) || (boardindex> MAX_BOARD_INDEX) )
    {
        cout << "Board index " << boardindex << \
                " has bad value; should be [0,7]" << endl;
        throw gpibDeviceException( devname, \
        "Error occured in getting board index ", \
        "Board index is out of range.", \
        "Value must be between 0 and 7", getiberr(), getibsta());
    }

    resetState();
    // Get Device handle from board index and device's primary addres;
    // return device handle (dev_handle)
    // N.B.
    //      Set the 3rd arg = secondary address to NO_SAD = 0 (= sec. address
    //                        disabled)
    //      Set the 4th arg = timeout to 10 seconds (T10s = 13),  the same
    //      as is the default value in the device server (DS) using this class.
    //      This timeout can later be changed with DS command SetTimeOut -->
    //      set_time_out(), which calls function setTimeOut() of this class.
    //      Set the 5th arg = EOT to 1, so that when writing to device the
    //      EOI will be asserted with the last data byte.
    //      Set the 6th arg = EOS to 0, which means we do not define any
    //      special end of string termination mode (i.e.EOS is disabled).
    //      Remark: Later EOT or/and EOS can be changed with setConfig() 
    //              function of this class.
    devhandle = ibdev(boardindex, primary_add, NO_SAD, T10s, 1, 0);
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
                ": An error occured while executing ibdev()."<< endl;
        throw gpibDeviceException( devname, \
        "Error occured in executing ibdev() to open GPIB device ",\
        iberrToString(), ibstaToString(), getiberr(), getibsta());
    }

    resetState();
    ibask(devhandle,IbaPAD,&pad);// Get PAD and check if it is the same as on ip
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
        ": An error occured executing ibask() to get primary address."<< endl;
        throw gpibDeviceException( devname, \
        "Error occured in asking for device Primary Address ", \
        iberrToString(), ibstaToString(), getiberr(), getibsta());
    }
    if (pad != primary_add)
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
        ": Primary address on i/p of constructor != returned by ibask()."<<endl;
        throw gpibDeviceException( dev_name, \
        "Error occured: primary address on the input of constructor not the same as obtained with ibask() ", \
        iberrToString(), ibstaToString(), getiberr(), getibsta());
    }

    // Now that all is OK, copy to internal gpibDevice class fields.
    dev_name    = devname;
    dev_handle  = devhandle;
    dev_pad     = pad;        // could use also primary_add
    dev_sad     = NO_SAD;
    board_index = boardindex;

    cout << "3rd gpibDevice constr: Leave " << endl;

};


//------------------ gpibDevice::gpibDevice() 4th -------------------
/**
 * This is the 4th constructor for the gpibDevice class.
 * The parameter passed to this constructor is:
 * - the device primary address, which should be one of [1,30].
 *
 * Since board name is not passed to this constructor, this constructor assumes
 * that this GPIB device is connected to the board with index 0 (i.e. that the
 * board name is "gpib0")!
 *
 * It opens a device by ibdev() call using device's primary address and
 * board name 'gpib0'.
 * This constructor assumes your GPIB device secondary address (the 3rd
 * argument to ibdev()) to be NO_SAD = 0, i.e. the secondary address is
 * disabled/unused!
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server used to instantiate a device (when device is 
 *        identified with its primary address).
 */
gpibDevice::gpibDevice(int primary_add)
{
    int    devhandle, pad, sad, boardindex;
    string devname;

    is_a_board   = false;
    debug_flag = false;

    cout << "4th gpibDevice constr: Enter " << endl;
    cout << "4th gpibDevice constr: GPIB Device Primary address = " << \
            primary_add << endl;
    cout << "4th gpibDevice constr: GPIB Board  name = gpib0" << endl;

    resetState();
    if ( (primary_add < 1) || (primary_add > gpib_addr_max) )
    {
        cout << "Primary address " << primary_add << \
                " has bad value; should be [1,30]" << endl;
        devname = "not-yet-defined";
        throw gpibDeviceException( devname, \
            "Error found in checking primary address ", \
            "Primary address is out of range.", \
            "Value must be between 1 and 30", getiberr(),getibsta());
    }

    boardindex = DEFAULT_BOARD_INDEX;

    // Since here do not have a meaningfull dev_name (it is an empty
    // string), compose it from device's primary and secondary addresses.
    // in order to use it in gpibDeviceException().
    // This allow us to leave gpibDeviceException.h and .cpp intact.
    // Use 0 for secondary address = sec. address disabled/not used.
    devname = padAndSadToString(primary_add, NO_SAD);

    resetState();
    // Get Device handle from Primary address with secondary address disabled
    // (NO_SAD = 0) and board index 0;
    // Return device handle (devhandle)
    // N.B.
    //      Set the 1st arg = board index to 0
    //      Set the 3rd arg = secondary address to NO_SAD=0(=sec.addr. disabled)
    //      Set the 4th arg = timeout to 10 seconds (T10s = 13),  the same
    //      as is the default value in the device server (DS) using this class.
    //      This timeout can later be changed with DS command SetTimeOut -->
    //      set_time_out(), which calls function setTimeOut() of this class.
    //      Set the 5th arg = EOT to 1, so that when writing to device the
    //      EOI will be asserted with the last data byte.
    //      Set the 6th arg = EOS to 0, which means we do not define any
    //      special end of string termination mode (i.e.EOS is disabled).
    //      Remark: Later EOT or/and EOS can be changed with setConfig() 
    //              function of this class.
    devhandle = ibdev(DEFAULT_BOARD_INDEX, primary_add, NO_SAD, T10s, 1, 0);
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = gpib0:" << devname << \
                ": An error occured while executing ibdev()."<< endl;
        throw gpibDeviceException( devname, \
            "Error occured in executing ibdev() to open GPIB device", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    resetState();
    ibask(devhandle,IbaPAD,&pad);// Get PAD and check if it is the same as on ip
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = gpib0:" << devname << \
        ": An error occured executing ibask() to get primary address."<< endl;
        throw gpibDeviceException( devname, \
            "Error occured in asking for device Primary Address ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }
    if (pad != primary_add)
    {
        cout << "Board:Device = gpib0:" << devname << \
        ": Primary Addres on input of constructor not the same as obtained with ibask" << endl;
        throw gpibDeviceException( devname, \
            "Error occured: Primary Address on the input of constructor not the same as obtained with ibask() ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    // Now that all is OK, copy to internal gpibDevice class fields.
    dev_name    = devname;
    dev_handle  = devhandle;
    dev_pad     = pad;        // could use also primary_add
    dev_sad     = NO_SAD;
    board_index = boardindex;

    cout << "4th gpibDevice constr: Leave " << endl;
};


//------------------ gpibDevice::gpibDevice() 5th -------------------
/**
 * This is the 5th constructor for the gpibDevice class.
 * The parameters passed to this constructor are:
 * - the device primary address, which should be one of [1,30].
 * - the device secondary address, which should be one of [96,126].
 * - the board name, where the device is connected to. The boardname is in a
 *   form 'gpibN', where N is a digit starting with 0.
 *
 * It opens a device by GPIB Board name, Primary and Secondary address.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server used to instantiate a device (when device is 
 *        identified with its primary, secondary address and a board name).
 */
gpibDevice::gpibDevice(int primary_add, int secondary_add, string boardname)
{
    int    devhandle, pad, sad, boardindex;
    string devname;
    string ss;

    is_a_board   = false;
    debug_flag = false;

    cout << "5th gpibDevice constr: Enter " << endl;
    cout << "5th gpibDevice constr: GPIB Device Primary   address = " << \
            primary_add << endl;
    cout << "5th gpibDevice constr: GPIB Device Secondary address = " << \
            secondary_add << endl;
    cout << "5th gpibDevice constr: GPIB Board  name              = " << \
            boardname << endl;

    resetState();
    if ( (primary_add < 1) || (primary_add > gpib_addr_max) )
    {
        cout << "Primary address " << primary_add << \
                " has bad value; should be [1,30]" << endl;
        devname = "not-yet-defined";
        throw gpibDeviceException( devname, \
            "Error found in checking primary address ", \
            "Primary address is out of range.", \
            "Value must be between 1 and 30", getiberr(),getibsta());
    }

    if ( (secondary_add != NO_SAD) && \
         ((secondary_add < 96) || (secondary_add > 126)) )
    {
        cout << "Secondary address " << secondary_add << \
                " has bad value; should be 0 or [96,126]" << endl;
        devname = "not-yet-defined";
        throw gpibDeviceException( devname, \
            "Error found in checking secondary address ", \
            "Secondary address is out of range.", \
            "Value must be 0 or between 96 and 126", getiberr(),getibsta());
    }

    resetState();
    // Extract board id (= board index).
    // In open-software Linux distribution (linux-gpib-x.y.z) the 
    // board name can be freely chosen in the file /etc/gpib.conf,
    // but to simplify code I stick to the convention to fix 
    // the board name to gpib0 for minor 0, to gpib1 for minor 1 etc.
    // So the code below for extracting board index is still valid.
    ss = boardname.substr(4);
    boardindex = atoi( ss.c_str() );

    // Since here do not have a meaningfull dev_name (it is an empty
    // string), compose it from device's primary and secondary addresses.
    // in order to use it in gpibDeviceException().
    // This allow us to leave gpibDeviceException.h and .cpp intact.
    devname = padAndSadToString(primary_add, secondary_add);

    if ((boardindex < DEFAULT_BOARD_INDEX) || (boardindex> MAX_BOARD_INDEX) )
    {
        throw gpibDeviceException( dev_name, \
            "Error occurs while getting device Addr ", \
            "Board index is out of range.", \
            "Value must be between 0 and 7", getiberr(),getibsta());
    }

    // Get Device from board index(1st arg), primary(2nd arg) and secondary
    // (3rd arg) addresses.
    // Return device handle (dev_handle)
    // N.B.
    //      Set the 4th arg = timeout to 10 seconds (T10s = 13),  the same
    //      as is the default value in the device server (DS) using this class.
    //      This timeout can later be changed with DS command SetTimeOut -->
    //      set_time_out(), which calls function setTimeOut() of this class.
    //      Set the 5th arg = EOT to 1, so that when writing to device the
    //      EOI will be asserted with the last data byte.
    //      Set the 6th arg = EOS to 0, which means we do not define any
    //      special end of string termination mode (i.e.EOS is disabled).
    //      Remark: Later EOT or/and EOS can be changed with setConfig() 
    //              function of this class.
    devhandle = ibdev(boardindex, primary_add, secondary_add, T10s, 1, 0);
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname << ":" << devname << \
                ": An error occured while executing ibdev()."<< endl;
#ifdef COM
	cout << "devhandle = " << devhandle;
	cout << "ibsta = " << dev_ibsta << " as string = " << ibstaToString() << endl;
	cout << "iberr = " << dev_iberr << " as string = " << iberrToString() << endl;
	return;
#endif
        throw gpibDeviceException( devname, \
            "Error occured in executing ibdev() to open GPIB device", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    resetState();
    ibask(devhandle,IbaPAD,&pad); // Get PAD and check if the same as on ip
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
        ": An error occured executing ibask() to get primary address."<< endl;
        throw gpibDeviceException( devname, \
            "Error occured in asking for device Primary Address ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }
    if (pad != primary_add)
    {
        cout << "Board:Device = " << boardname << ":" << devname << \
        ": Primary Addres on input of constructor not the same as obtained with ibask" << endl;
        throw gpibDeviceException( devname, \
            "Error occured: Primary Address on the input of constructor not the same as obtained with ibask() ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    resetState();
    ibask(devhandle,IbaSAD,&sad);   // Get SAD and check if the same as on ip
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = " << boardname <<":" << devname << \
        ": An error occured executing ibask() to get secondary address."<< endl;
        throw gpibDeviceException( devname, \
            "Error occured executing ibask() to get secondary address.", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }
    if (sad != secondary_add)
    {
        cout << "Board:Device = " << boardname << ":" << devname << \
        ": Secondary Addres on input of constructor not the same as obtained with ibask" << endl;
        throw gpibDeviceException( devname, \
            "Error occured: Secondary Address on the input of constructor not the same as obtained with ibask() ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    // Now that all is OK, copy to internal gpibDevice class fields.
    dev_name    = devname;
    dev_handle  = devhandle;
    dev_pad     = pad;          // could use also primary_add
    dev_sad     = sad;          // could use also scondary_add
    board_index = boardindex;

    cout << "5th gpibDevice constr: Leave " << endl;
};


//------------------ gpibDevice::gpibDevice() 6th -------------------
/**
 * This is the 6th constructor for the gpibDevice class.
 * The parameters passed to this constructor are:
 * - the device primary address, which should be one of [1,30].
 * - the device secondary address, which should be one of [96,126].
 *
 * Since board name is not passed to this constructor, this constructor assumes
 * that this GPIB device is connected to the board with index 0 (i.e. that the
 * board name is "gpib0")!
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server used to instantiate a device (when device is 
 *        identified with its primary and secondary address).
 */
gpibDevice::gpibDevice(int primary_add, int secondary_add)
{
    int    devhandle, pad, sad, boardindex;
    string devname;

    is_a_board   = false;
    debug_flag = false;

    cout << "6th gpibDevice constr: Enter " << endl;
    cout << "6th gpibDevice constr: GPIB Device Primary   address (on board 0) = " << primary_add << endl;
    cout << "6th gpibDevice constr: GPIB Device Secondary address (on board 0) = " << secondary_add << endl;

    resetState();
    if ( (primary_add < 1) || (primary_add > gpib_addr_max) )
    {
        cout << "Primary address " << primary_add << \
                " has bad value; should be [1,30]" << endl;
        devname = "not-yet-defined";
        throw gpibDeviceException( devname, \
            "Error found in checking primary address ", \
            "Primary address is out of range.", \
            "Value must be between 1 and 30", getiberr(),getibsta());
    }

    if ( (secondary_add != NO_SAD) && \
         ((secondary_add < 96) || (secondary_add > 126)) )
    {
        cout << "Secondary address " << secondary_add << \
                " has bad value; should be 0 or [96,126]" << endl;
        devname = "not-yet-defined";
        throw gpibDeviceException( devname, \
            "Error found in checking secondary address ", \
            "Secondary address is out of range.", \
            "Value must be 0 or between 96 and 126", getiberr(),getibsta());
    }

    boardindex = DEFAULT_BOARD_INDEX;

    resetState();
    // Since here do not have a meaningfull dev_name (it is an empty
    // string), compose it from device's primary and secondary addresses.
    // in order to use it in gpibDeviceException().
    // This allow us to leave gpibDeviceException.h and .cpp intact.
    devname = padAndSadToString(primary_add, secondary_add);

    resetState();
    // Get Device handle from Primary and Secondary address on board 0.
    // Return device handle (devhandle)
    // N.B.
    //      Set the 1st arg = board index to 0
    //      Set the 4th arg = timeout to 10 seconds (T10s = 13),  the same
    //      as is the default value in the device server (DS) using this class.
    //      This timeout can later be changed with DS command SetTimeOut -->
    //      set_time_out(), which calls function setTimeOut() of this class.
    //      Set the 5th arg = EOT to 1, so that when writing to device the
    //      EOI will be asserted with the last data byte.
    //      Set the 6th arg = EOS to 0, which means we do not define any
    //      special end of string termination mode (i.e.EOS is disabled).
    //      Remark: Later EOT or/and EOS can be changed with setConfig() 
    //              function of this class.
    devhandle = ibdev(boardindex, primary_add, secondary_add, T10s, 1, 0);
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = gpib0:" << devname << \
                ": An error occured while executing ibdev()."<< endl;
        throw gpibDeviceException( devname, \
            "Error occured in executing ibdev() to open GPIB device", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    resetState();
    ibask(devhandle,IbaPAD,&pad);  // Get PAD and check if the same as on ip
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = gpib0:" << devname << \
        ": An error occured executing ibask() to get primary address."<< endl;
        throw gpibDeviceException( devname, \
            "Error occured in asking for device Primary Address ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }
    if (pad != primary_add)
    {
        cout << "Board:Device = gpib0:" << devname << \
        ": Primary Addres on input of constructor not the same as obtained with ibask" << endl;
        throw gpibDeviceException( devname, \
            "Error occured: Primary Address on the input of constructor not the same as obtained with ibask() ",
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    resetState();
    ibask(devhandle,IbaSAD,&sad);   // Get SAD and check if the same as on ip
    saveState();
    if ( (devhandle & ERR) || (dev_ibsta & ERR) )
    {
        cout << "Board:Device = gpib0:" << devname << \
        ": An error occured executing ibask() to get secondary address."<< endl;
        throw gpibDeviceException( devname, \
            "An error occured executing ibask() to get secondary address ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }
    if (sad != secondary_add)
    {
        cout << "Board:Device = gpib0:" << devname << \
        ": Secondary Addres on input of constructor not the same as obtained with ibask" << endl;
        throw gpibDeviceException( devname, \
            "Error occured: Secondary Address on the input of constructor not the same as obtained with ibask() ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta());
    }

    // Now that all is OK, copy to internal gpibDevice class fields.
    dev_name    = devname;
    dev_handle  = devhandle;
    dev_pad     = pad;          // could use also primary_add
    dev_sad     = sad;          // could use also scondary_add
    board_index = boardindex;

    cout << "6th gpibDevice constr: Leave " << endl;
};

// Remark: There is no destructor defined here.
// TODO: Maybe provide it and do inside the same operations as in
//       setOffLine() function.


//------------------ gpibDevice::setOffLine() -------------------
/**
 * This method should be called to 'free/close' the device/board after use.
 * When device/board is closed, the file descriptor (handle) is returned
 * to the system and is available for future ibconf() or/and ibdev().
 *
 * If device/board should be used again, then for the board the 2nd constructor
 * and for device any one of 6 constructors (on beamlines 3->6) should be
 * invoked to execute either ibconf() or ibdev() in order to 'open'
 * device/board again.
 *
 * This function is placed here, since it does 'symmetrical' operation
 * with respect to constructors.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
void gpibDevice::setOffLine(void)
{
    if (debug_flag == true) cout << "setOffLine(): Enter " << endl;

    resetState();
    ibonl(dev_handle, 0);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "setOffLine(): Error in closing device/board " << \
                    dev_name << " with setOffLine() method(ibonl)" << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error in closing device/board with setOffLine method(ibonl) ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    dev_handle = -1; // set dev_handle to invalid value.
    if (debug_flag == true) cout << "setOffLine(): Leave " << endl;
}



// debug - related public functions:
// ---------------------------------

//------------------ gpibDevice::setDebug() -------------------
/**
 * This method allows to set debug flag to true or false.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        TODO: In device server it will be used on a device object(gpibDevice)
 *              as well as on a controller object (gpibBoard).
 */
void gpibDevice::setDebug(bool dbg)
{
    debug_flag = dbg;
}


//------------------ gpibDevice::getDebug() -------------------
/**
 * This method allows to get debug flag value
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        TODO: In device server it will be used on a device object(gpibDevice)
 *              as well as on a controller object (gpibBoard).
 */
bool gpibDevice::getDebug(void)
{
    return debug_flag;
}



// status and error - related public functions:
// --------------------------------------------

//------------------ gpibDevice::ibstaToString() -------------------
/**
 * ibsta register string conversion.
 * This method returns string describing ibsta register meaning.
 *
 * Usage: It is used internally (= in this class (gpibDevice)
 *        and the derived class (gpibBoard)).
 *        It is not used by device server directly but get the string
 *        through exception.
 */
string gpibDevice::ibstaToString(void)
{
    string ret;

    if (dev_ibsta & ERR)   ret  = "GPIB error ";
    if (dev_ibsta & TIMO)  ret += "Time limit exceeded ";
    if (dev_ibsta & END)   ret += "EOI or EOS detected ";
    if (dev_ibsta & SRQI)  ret += "SRQ Interrupt received by CIC";
    if (dev_ibsta & RQS)   ret += "Device requesting service ";
    if (dev_ibsta & SPOLL) ret += "board serial polled by busmaster ";
    if (dev_ibsta & EVENT) ret += "DCAS, DTAS, or IFC has occured ";
    if (dev_ibsta & CMPL)  ret += "DMA I/O completed (SHAH synched) ";
    if (dev_ibsta & LOK)   ret += "Local lockout state ";
    if (dev_ibsta & REM)   ret += "Remote state ";
    if (dev_ibsta & CIC)   ret += "Controller-In-Charge ";
    if (dev_ibsta & ATN)   ret += "Attention is asserted ";
    if (dev_ibsta & TACS)  ret += "Talker active ";
    if (dev_ibsta & LACS)  ret += "Listener active";
    if (dev_ibsta & DTAS)  ret += "Device trigger state ";
    if (dev_ibsta & DCAS)  ret += "Device clear state ";

    return ret;
}


//------------------ gpibDevice::iberrToString() -------------------
/**
 * iberr register string conversion.
 *
 * When ibsta returns ERR = 1<<15, then iberr contains more precise
 * error condition.
 *
 * When iberr = EDVR error = Unix/Linux system error, then
 * ibcnt contains the precise Unix/Linux error code.
 *
 * This method returns error string corresponding to iberr register and
 * in case this one is EDVR it gives also precise Unix/Linux system error.
 *
 * Usage: It is used internally (= in this class (gpibDevice)
 *        and the derived class (gpibBoard)).
 *        It is not used by device server directly but get the string
 *        through exception.
 */
string gpibDevice::iberrToString(void) {

    string ret;

    if ( (dev_iberr >= ECIC) && (dev_iberr <= ETAB) )
    {
        ret = error_array[dev_iberr];
    }
    else if (dev_iberr == EDVR)   // Unix/Linux system error
    {
        ret = error_array[dev_iberr] + std::strerror(dev_ibcnt);
    }
    else
    {
        ret = "unknown error"; // in fact this one is impossible
    }
    return ret;
}


//------------------ gpibDevice::getiberr() -------------------
/**
 * This method returns device's iberr register (numerical value of error).
 *
 * Usage: It is used internally (= inside other functions of this class
 *        (gpibDevice) and derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
int gpibDevice::getiberr(void)
{
    return dev_iberr;
}


//------------------ gpibDevice::getibsta() -------------------
/**
 * This method returns device's ibsta register (numerical value of status).
 *
 * Usage: It is used internally (= inside other functions of this class
 *        (gpibDevice) and derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
int gpibDevice::getibsta(void)
{
    return dev_ibsta;
}


//------------------ gpibDevice::getibcnt() -------------------
/**
 * This method returns device's ibcnt register.
 *
 * Usage: It is used internally (= inside other functions of this class
 *        (gpibDevice) and derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
unsigned int gpibDevice::getibcnt(void)
{
    return dev_ibcnt;
}



// some informative public functions
// ---------------------------------

//------------------ gpibDevice::getName() -------------------
/**
 * This method returns device's name.
 * This can be the name passed to the 1st or 2nd constructor or the name
 * built from primary and secondary addresses.
 * In the case the 2nd constructor is used in the derived class (gpibBoard),
 * this variable returns board name.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
string gpibDevice::getName(void)
{
    if (debug_flag == true)
    {
        cout << "getName(): Enter " << endl;
        if (is_a_board == true)
        {
            cout << "getName(): Board name is : " << dev_name << endl;
        }
        else
        {
            cout << "getName(): Device name is : " << dev_name << endl;
        }
        cout << "getName(): Leave " << endl;
    }
    return dev_name;
}


//------------------ gpibDevice::getHandle() -------------------
/**
 * This method returns device's or board handle.
 * For ex. if at higher level (like DS) after instantiating a board
 * and when want to execute an operation on a given device, this function
 * allows to obtain device handle to be then used in a board function.
 * For example : Method BCclr needs this value to clear a device.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
int gpibDevice::getHandle(void)
{
    if (debug_flag == true)
    {
        cout << "getHandle(): Enter " << endl;
        if (is_a_board == true)
        {
            cout << "getHandle(): Board handle is : " << dev_handle << endl;
        }
        else
        {
            cout << "getHandle(): Device handle is : " << dev_handle << endl;
        }
        cout << "getHandle(): Leave " << endl;
    }
    return dev_handle;
}


//------------------ gpibDevice::getPad() -------------------
/**
 * This method returns device's or board's Primary Address.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
int gpibDevice::getPad(void)
{
    if (debug_flag == true)
    {
        cout << "getPad(): Enter " << endl;
        if (is_a_board == true)
        {
            cout << "getPad(): Board Primary Address is : " << dev_pad << endl;
        }
        else
        {
            cout << "getPad(): Device Primary Address is : " << dev_pad << endl;
        }
        cout << "getPad(): Leave " << endl;
    }
    return dev_pad;
}


//------------------ gpibDevice::getSad() -------------------
/**
 * This method returns device's or board's Secondary Address.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
int gpibDevice::getSad(void)
{
    if (debug_flag == true)
    {
        cout << "getSad(): Enter " << endl;
        if (is_a_board == true)
        {
            cout << "getSad(): Board Secondary Address is : " << dev_sad <<endl;
        }
        else
        {
            cout << "getSad(): Device Secondary Address is : " << dev_sad<<endl;
        }
        cout << "getSad(): Leave " << endl;
    }
    return dev_sad;
}


//------------------ gpibDevice::getBoardIndex() -------------------
/**
 * This method returns board's index for a given board
 * 
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a controller object (gpibBoard) 
 *        and not on a device object (gpibDevice), although it can be
 *        applied to both.
 */
int gpibDevice::getBoardIndex(void)
{
    if (debug_flag == true)
    {
        cout << "getBoardIndex(): Enter " << endl;
        cout << "getBoardIndex(): Board Index is : " << board_index << endl;
        cout << "getBoardIndex(): Leave " << endl;
    }
    return board_index;
}



// configuration - related public functions
// ----------------------------------------

//------------------ gpibDevice::setTimeOut() -------------------
/**
 * Set the device/board time out value.
 * The timeout value determines how long I/O routines like ibrd/ibwrt
 * wait for a response from a device/board.
 * Warning: These values are predefined with #define statements in ugpib.h.
 *          Not all values allowed, but just 18 time out values from the
 *          above mentioned header file.
 * TODO: For clarity in debug or/and error print prepare time string for
 *       a given timeout constant.
 * Remark: Timeout can also be set with setConfig() function by giving
 *         one of 18 possible values for option IbcTMO.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
void gpibDevice::setTimeOut(int tmo)
{
    if (debug_flag == true) cout << "setTimeOut(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "setTimeOut(): Will set timeout on " << dev_name << \
                " to value(num. index) " << tmo << endl;
    }

    resetState();
    if ( (tmo >= TNONE) && (tmo <= T1000s) )
    {
        ibtmo(dev_handle,tmo);
        saveState();
    } 
    else 
    {
        if (debug_flag == true)
        {
            cout << "setTimeOut(): Timeout value " << tmo << \
                    " out of range[0,17] for " << dev_name << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured in setting time out ", \
            "Value out of range.", " [0-17] value expected.", \
            getiberr(), getibsta() );
    }

    // tmo contains correct value, does the command ibtmo() terminate correctly?
    if ( dev_ibsta & ERR )
    {
        if (debug_flag == true)
        {
            cout << "setTimeOut(): Error in setting timeout on " << \
                    dev_name << " with ibtmo() method" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occurs while setting time out ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "setTimeOut(): Leave " << endl;
}


//------------------ gpibDevice::getTimeOut() -------------------
/**
 * Get the device/board time out value.
 * The timeout value determines how long I/O routines like ibrd/ibwrt
 * wait for a response from a device/board.
 * To get time out value used ibask() function with appropriate code (IbaTMO).
 * The value returned is one of the 18 possible time out code (as predefined
 * in ugpib.h). Then it can be converted to seconds in calling environement 
 * (for ex. device server).
 * Remark: Timeout can also be obtained with getConfig() function by using
 *         code IbaTMO.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
int gpibDevice::getTimeOut(void)
{
    int value;

    if (debug_flag == true) cout << "getTimeOut(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "getTimeOut(): Will get timeout for " << dev_name << endl;
    }

    resetState();
    ibask(dev_handle, IbaTMO, &value);
    saveState();
    if ( dev_ibsta & ERR )
    {
        if (debug_flag == true)
        {
            cout << "getTimeOut(): Error in getting timeout on " << \
                    dev_name << " with ibask() method" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occurs while getting time out ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    cout << "getTimeOut(): Got timeout value(code) " << value << \
            " for device " << dev_name << endl;
    if (debug_flag == true) cout << "getTimeOut(): Leave " << endl;
    return value;
}


//------------------ gpibDevice::setConfig() -------------------
/**
 * This method sends a configuration request to device or board using
 * NI library function ibconfig().
 * Warning: Not all configuration options are possible for both device/board,
 *          some are common, some only for devices and some only for a board.
 *          For the moment we do not go into detailed checking if given
 *          option is suitable or not.
 *          TODO: can refine later this function by adding a test:
 *                if (is_a_board) ....
 * Remark: in the header files ugpib.h/ni488.h the prototype ibconfig() uses
 *         int type for all 3 arguments while in the NI doc or 'Measurement
 *         Computing' doc the prototype of ibconfig() would use types:
 *         int, unsigned int, unsigned int. The DS when using the ibconfig()
 *         function uses long type for option and value. Seems that long
 *         will automatically convert to int in the call to ibconfig().
 *         So for the moment will leave as it is (i.e. ints here and long in DS)
 *         and if necessary will modify later.
 *         TODO: see if there are compiler warnings and later if need to
 *               modify types for option and value here and DS.
 *         TODO: For clarity in debug or/and error print prepare option 
 *               string for a given option constant.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used both on a device object (gpibDevice) 
 *        and on a controller object (gpibBoard).
 */
void gpibDevice::setConfig(int option, int value)
{
    if (debug_flag == true) cout << "setConfig(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "setConfig(): On " << dev_name << \
                " Will set option(num. index) " << option << \
                " to value " << value << endl;
    }
    resetState();
    ibconfig(dev_handle, option, value);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "setConfig(): Error in setting config option " << option <<\
                    " on " << dev_name << " with ibconfig() method" <<endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured in setting config ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "setConfig(): Leave " << endl;
}


//------------------ gpibDevice::getConfig() -------------------
/**
 * This method asks for a value of a given configuration parameter of
 * device or board using NI library function ibask().
 * Warning: Not all configuration options are possible for both device/board,
 *          some are common, some only for devices and some only for a board.
 *          For the moment we do not go into detailed checking if given
 *          option is suitable or not.
 *          TODO: can refine later this function by adding a test:
 *                if (is_a_board) ....
 * Remark: in the header files ugpib.h/ni488.h the prototype ibask() uses
 *         int type for all 3 arguments (the 3rd one is ptr to int) and returns
 *         int, while in the NI doc or 'Measurement Computing' doc the
 *         prototype of ibask() would use types:
 *         int, int, unsigned int *. The DS when using the ibask()
 *         function uses short type for option and value!
 *         For the moment will leave as it is (i.e. ints here and shorts in DS)
 *         and if necessary will modify later.
 *         TODO: see if there are compiler warnings and later if need to
 *               modify types for option and value here and DS.
 *         TODO: For clarity in debug or/and error print prepare option 
 *               string for a given option constant.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used both on a device object (gpibDevice) 
 *        and on a controller object (gpibBoard).
 */
int gpibDevice::getConfig(int option)
{
    int value;

    if (debug_flag == true) cout << "getConfig(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "getConfig(): On " << dev_name << \
                " Will ask for a value of option (num. index) "<< option <<endl;
    }
    resetState();
    ibask(dev_handle, option, &value);
    if (debug_flag == true)
    {
        cout << "getConfig(): option = " << option << \
                             " value = " << value << endl;
    }
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "getConfig(): Error in getting config option " << option <<\
                    " on " << dev_name << " with ibask() method" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured while asking for one configuration field of GPIB board or device ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "getConfig(): Leave " << endl;
    return value;
}

// Remark: For the moment we do not create any public function in gpibDevice
//         class which would use the remaining confuguration-related functions
//         from GPIB library: ibpad, ibsad,  ibeos, ibeot, ibdma, ibrsc.
//         (so from the whole set we use only: ibtmo, ibconfig and ibask).



// device-control - related public functions
// -----------------------------------------

//------------------ gpibDevice::getSerialPollByte() -------------------
/**
 * This method makes a serial poll on the GPIB device.
 * Serial poll response byte of device is device-specific with the 
 * exception of bit 6. If bit 6 (0x40) is set, the a device is requesting 
 * service.
 * Remark: Alternative is to use on gpibDevice object the following
 *         NI-488.2 library function:
 *       ReadStatusByte(board_index, MakeAddr(dev_pad, dev_sad), short *result);
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), since NI-488.1 function
 *        ibrsp() is applicable only to a GPIB device and not a controller.
 */
short gpibDevice::getSerialPollByte(void)
{
    char serialpollbyte;
    if (debug_flag == true) cout << "getSerialPollByte(): Enter " << endl;
    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "getSerialPollByte(): " << dev_name << \
                    " This function is only for a device, not a board" << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured when want to serial poll device ", \
            "Intended to do it on Board.", \
            "Can be used only on a Device", getiberr(),getibsta());
    }
    if (debug_flag == true)
    {
        cout << "getSerialPollByte():" << \
                " Will serial poll device " << dev_name << endl;
    }
    resetState();
    ibrsp(dev_handle,&serialpollbyte );
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "getSerialPollByte(): Error in serial poll device " << \
                    dev_name << " with ibrsp() method" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured while reading serial poll byte ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true)
    {
        cout << "getSerialPollByte(): on device " << dev_name << \
                " Serial poll byte = " << (short)serialpollbyte << endl;
    }
    if (debug_flag == true) cout << "getSerialPollByte(): Leave " << endl;
    return (short)serialpollbyte;
}


//------------------ gpibDevice::clear() -------------------
/**
 * This method clears a GPIB device.
 * Remark1: There is also function clearDev() at the board level
 *          (i.e. in gpibBoard class, derived from gpibDevice class)
 * Remark2: Alternative is to use on gpibDevice object the following
 *          NI-488.2 library function:
 *          DevClear(board_index, MakeAddr(dev_pad, dev_sad));
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), since NI-488.1 function
 *        ibclr() is applicable only to a GPIB device and not a controller.
 */
void gpibDevice::clear(void)
{
    if (debug_flag == true) cout << "clear(): Enter " << endl;
    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "clear(): " << dev_name << \
                    " This function is only for a device, not a board" << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured when want to clear device ", \
            "Intended to do it on Board.", \
            "Can be used only on a Device", getiberr(),getibsta());
    }
    if (debug_flag == true)
    {
        cout << "clear(): Will clear device " << dev_name << endl;
    }
    resetState();
    ibclr(dev_handle);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "clear(): Error in claring device " << dev_name << \
                    " with ibclr() method" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occurs while clearing to GPIB ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "clear(): Leave " << endl;
}


//------------------ gpibDevice::trigger() -------------------
/**
 * This method sends a trigger signal to a GPIB device. The device was
 * previously set to wait for a trigger command. When it receives the signal,
 * the device sends its measurement data. You can get it with read Command.
 * Remark1: There is no equivalent function in gpibBoard() class like for
 *          clearing device.
 * Remark2: Alternative is to use on gpibDevice object the following
 *          NI-488.2 library function:
 *          Trigger(board_index, MakeAddr(dev_pad, dev_sad));
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), since NI-488.1 function
 *        ibtrg() is applicable only to a GPIB device and not a controller.
 */
void gpibDevice::trigger(void)
{
    if (debug_flag == true) cout << "trigger(): Enter " << endl;
    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "trigger(): " << dev_name << \
                    " This function is only for a device, not a board" << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured when want to trigger device ", \
            "Intended to do it on Board.", \
            "Can be used only on a Device", getiberr(),getibsta());
    }
    if (debug_flag == true)
    {
        cout << "trigger(): Will trigger device " << dev_name << endl;
    }
    resetState();
    ibtrg(dev_handle);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "trigger(): Error sending trigger to device " << \
                    dev_name << " with ibtrg() method" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured in triggering device ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "trigger(): Leave " << endl;
}


/**
 * This method test if the GPIB device is alive (is a listener) on the bus.
 * If returns 0: nobody there, else returns 1, which means OK (device is there)
 *
 * IMPORTANT NOTE:
 * There is a lot of discussions on various forums since the doc on
 * this function is confusing and some users found strange behaviour
 * of this function with certain GPIB controller boards (either they
 * found devices reported to be absent although they were there or
 * they got devices reported to be present where they were not there!)
 *
 * Remark: To check if a device is a listener (i.e. "alive"), alternative
 *         to ibln() from NI-488.1 library is to use the FindLstn() NI-488.2
 *         library function. For ex. if want to test if device with
 *         primary address 6 is alive, need to do:
 *         FindLstn(board_index, scanlist, resultlist, gpib_addr_max + 1);
 *         where scanlist is NOADDR-terminated list of devices to be checked
 *         as listeners, which in this case has only 1 device address:
 *         scanlist[0] = 6;
 *         scanlist[1] = NOADDR;
 *         After this must check in the resultlist if get the 6-th field
 *         non-zero indicating that the device with primary address 6 is alive.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard)).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), since NI-488.1 function
 *        ibln() is applicable only to a GPIB device and not a controller.
 */
short gpibDevice::isAlive(void) {

    if (debug_flag == true) cout << "isAlive(): Enter " << endl;

    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "isAlive(): " << dev_name << \
                    " This function is only for a device, not a board" << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured when want to check if device is alive ", \
            "Intended to do it on Board though this is devices method.", \
            "Can be used only on a Device", getiberr(),getibsta());
    }
    if (debug_flag == true)
    {
        cout << "isAlive(): Will make listener check for device " << \
                dev_name << endl;
    }
    resetState();
    if (debug_flag == true)
    {
        //cout << "isAlive(): dev_handle = " << dev_handle << endl;
        cout << "isAlive(): board_index = " << board_index << endl;
        cout << "isAlive(): dev_pad = " << dev_pad << endl;
        cout << "isAlive(): dev_sad = " << dev_sad << endl;
    }
    // Remark: In RHE Linux need to pass device handle,
    //         while in Open-Software Linux for Debian (linux-gpib-x.y.z)
    //         need to use board index.
    //ibln( dev_handle , dev_pad, dev_sad, &alive);
    ibln( board_index , dev_pad, dev_sad, &alive);
    if (debug_flag == true) 
    {
        cout << "isAlive(): alive = " << alive << endl;
    }
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "isAlive(): Error checking if device " << dev_name << \
                    " is alive with ibln() method" << endl;
        }
        throw gpibDeviceException( dev_name,\
            "Device not answering to ibln (isAlive() method).", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }

    if (debug_flag == true) cout << "isAlive(): Leave " << endl;
    return alive;
}



// Remark: For the moment we do not create any public function in gpibDevice
//         class which would use the remaining device control-related functions
//         from GPIB library: ibwait, ibppc and ibpct
//         (so from the whole set we use only: ibrsp, ibclr, ibtrg, ibln and
//          ibloc)



// device/board control - related public functions
// -----------------------------------------------

//------------------ gpibDevice::goToLocalMode() -------------------
/**
 * This method tries to put the device/board in local control mode in order
 * to control it manually/locally.
 * When used on gpibDevice object it acts on device (dev_handle is dev.handle)
 * and when used on gpibBoard object it acts on board (dev_handle is board h.).
 * Sending commands to the device automatically turns it back to remote mode if
 * REN is asserted.
 * WARNING: This method can conflict with device server polling since
 *          network access to device via DS automatically put the device
 *          in remote mode [TODO: what about board?].
 *          Due to this will probably never use this method.
 * Remark1: For device(s), alternative is to use on gpibBoard object
 *          the following NI-488.2 library function:
 *          EnableLocal(board_index, addrlist);
 *          where addrlist is NOADDR-terminated list of devices to be put
 *          in local mode. For ex.
 *          addrlist[0] = 6;
 *          addrlist[1] = 7;
 *          addrlist[2] = NOADDR;
 *          would set devices with primary addresses 6 and 7 to local mode.
 * Remark2: Another alternative, if want to set ALL connected devices into 
 *          local mode would be to create bus-control related function for 
 *          gpibBoard class which would internally call:
 *          ibsre(dev_handle, 0)
 *          Since the 2nd argument is 0, this unasserts REN line, hence puts
 *          all connected devices into local mode.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
void gpibDevice::goToLocalMode(void)
{
    if (debug_flag == true) cout << "goToLocalMode(): Enter " << endl;
    if (debug_flag == true)
    {
        if (is_a_board == true)
        {
            cout << "goToLocalMode(): Will set board " << dev_name << \
                    " in local mode" << endl;
        }
        else
        {
            cout << "goToLocalMode(): Will set device " << dev_name << \
                    " in local mode" << endl;

        }
    }
    resetState();
    ibloc(dev_handle);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            if (is_a_board == true)
            {
                cout << "goToLocalMode(): Error setting board " << dev_name << \
                        " to local mode with ibloc() method" << endl;
            }
            else
            {
                cout << "goToLocalMode(): Error setting device " << dev_name<< \
                        " to local mode with ibloc() method" << endl;
            }
        }
        throw gpibDeviceException( dev_name, \
            "Error occured with ibloc() command", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "goToLocalMode(): Leave " << endl;
}

// Remark: For the moment we do not create any public function in gpibDevice
//         class which would use the remaining device/board control-related
//         functions from GPIB library: ibwait, ibppc, ibrsv and ibist.
//         (so from the whole set we use only ibloc)



// bus control - related public functions
// --------------------------------------

//------------------ gpibDevice::goToRemoteMode() -------------------
/**
 * This method tries to put the device in remote control mode.
 * (Sending commands to the device automatically turns it into remote mode).
 * This method is provided for backward compatibility and will probably
 * never use it.
 *
 * In order to keep old signature of this function and to use it on the
 * gpibDevice object, will use NI-488.2 function EnableRemote() which uses
 * board_index(known at gpibDevice object level) as first argin. 
 * 
 * The function ibsre (as used originally) cannot be used here, since it 
 * needs a board handle as the 1st argument and device object does not know 
 * it [one solution would be that a client like device server first calls 
 * getHandle() on a board object and pass it to this function, of which we 
 * would then have to change the signature]. Moreover with ibsre() CANNOT
 * SELECTIVELY put only one device to remote mode. By asserting REN line it 
 * automatically sets to remote mode ALL connected devices.
 *
 * To set ALL connected devices to remote mode, we added a function
 * setAllToRemoteMode() in the gpibBoard() class.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), since with keeping
 *        the function signature we cannot pass device's primary address,
 *        which is unnecessary at device level, but would be needed at 
 *        gpibBoard level.
 */
void gpibDevice::goToRemoteMode(void)
{
    Addr4882_t addrlist[2];

    if (debug_flag == true) cout << "goToRemoteMode(): Enter " << endl;

    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "goToRemoteMode(): " << dev_name << \
            " This function is to be applied on a device, not on a board"<<endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured when want to execute goToRemoteMode ", \
            "Intended to do it on Board.", \
            "Can be used only on a Device object", getiberr(),getibsta());
    }
    if (debug_flag == true)
    {
        cout << "goToRemoteMode(): Will set device " << dev_name << " into remote mode" << endl;
    }

    addrlist[0] = dev_pad;
    addrlist[1] = NOADDR;
    resetState();
    EnableRemote(board_index, addrlist);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "goToRemoteMode(): Error setting device " << dev_name << \
		    " to remote mode " << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured with EnableRemote() command", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "goToRemoteMode(): Leave " << endl;
}

// Remark: For the moment we do not create any public function in gpibDevice
//         class which would use the bus control-related
//         functions from GPIB library: ibsic, ibcac, ibgts, ibsre, ibcmd, 
//         ibcmda, ibrpp and iblines.
//         but we use ibcmd and ibsre in gpibBoard() class.


// IO - related public functions
// -----------------------------

//------------------ gpibDevice::write() -------------------
/**
 * This method writes data from a string to GPIB device or board.
 * Return the number of bytes written or -1 if problem.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
int gpibDevice::write(string m)
{
    int bytes_written = -1;
    if (debug_flag == true) cout << "write(): Enter " << endl;
    if (debug_flag == true)
    {
        if (is_a_board == true)
        {
            cout << "write(): Will write string " << m << " to board " \
                 << dev_name << endl;
        }
        else
        {
            cout << "write(): Will write string " << m << " to device " \
                 << dev_name << endl;
        }
    }
    resetState();
    ibwrt( dev_handle, (char *)m.c_str(), m.length() );
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            if (is_a_board == true)
            {
                cout << "write(): Error writing to board " << dev_name << \
                        " with ibwrt() method" << endl;
            }
            else
            {
                cout << "write(): Error writing to device " << dev_name << \
                        " with ibwrt() method" << endl;
            }
        }
        throw gpibDeviceException( dev_name, \
            "Error occured while writing to GPIB ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    bytes_written = dev_ibcnt; /* saved ibcnt value */
    if (debug_flag == true) cout << "write(): Leave " << endl;
    return bytes_written;
}


//------------------ gpibDevice::read() 1st ----------------
/**
 * This method reads string from a GPIB device or board, when we
 * expect that the string length will not exceed RD_BUFFER_SIZE.
 * Returns the string read.
 * dev_ibcnt contains number of bytes read and is valid untill another call
 * to a NI-488.1 function. The calling environement can use getibcnt() to 
 * get it, but there is no need since string.length() already does that.
 * Remark: Coders should use this method rather than read(int) because 
 *         its buffer of RD_BUFFER_SIZE size is almost enough all time. 
 *         Only use read(int) when you expect return buffer > RD_BUFFER_SIZE 
 *         ( but this method is slowest due to dynamic buffer allocation).
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
string gpibDevice::read(void)
{
    string ret;

    if (debug_flag == true) cout << "read() - 1st: Enter " << endl;

    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "read() - 1st: Will read max 512 chars from board " << dev_name << endl;
        }
    }
    else
    {
        if (debug_flag == true)
        {
            cout << "read() - 1st: Will read max 512 chars from device " << dev_name << endl;
        }
    }
    resetState();
    memset(rd_buffer,0, (RD_BUFFER_SIZE+1));
    ibrd(dev_handle,rd_buffer,RD_BUFFER_SIZE);
    saveState();

    //ret = rd_buffer;
    ret = string(rd_buffer, dev_ibcnt);

    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "read() - 1st: Error reading from  " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured while reading from GPIB ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "read() - 1st: Leave " << endl;
    return ret;
}


//------------------ gpibDevice::writeRead() -------------------
/**
 * This method is a combination of previous two done in the same
 * function one after the other. It's a kind of query: first write
 * is performed and then read. For ex.
 * With write would sent a string *IDN? and
 * with read would read response back (device ID string)
 * Remark: As in the read() function above we that the length of the
 *         string read out from device will not exceed RD_BUFFER_SIZE.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
string gpibDevice::writeRead(string m)
{
    string ret;

    if (debug_flag == true) cout << "writeRead(): Enter " << endl;
    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "writeRead() : Will write and then read max 512 chars from board " << dev_name << endl;
        }
    }
    else
    {
        if (debug_flag == true)
        {
            cout << "writeRead(): Will write and then read max 512 chars from device " << dev_name << endl;
        }
    }

    // Make the First Operation: Write.
    resetState();
    ibwrt(dev_handle,(char *) m.c_str(),m.length() );
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "writeRead(): Error writing to " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occurs while writing to GPIB ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }

    // Make the Second operation: Read.
    resetState();
    memset(rd_buffer,0, (RD_BUFFER_SIZE+1));
    ibrd(dev_handle,rd_buffer,RD_BUFFER_SIZE);
    saveState();

    // dev_ibcnt contains string length.
    ret = string(rd_buffer, dev_ibcnt);

    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "writeRead(): Error reading from  " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occurs while reading from GPIB ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "writeRead(): Leave " << endl;
    return ret;
}


//------------------ gpibDevice::read() 2nd ----------------
/**
 * This method reads data from a GPIB device or board, where a size 
 * is specified in input argument, while in the first read() function
 * above we used the predefined size (= RD_BUFFER_SIZE).
 * Returns the string read.
 * dev_ibcnt contains number of bytes read and is valid untill another call
 * to a NI-488.1 function. The calling environement can use getibcnt() to 
 * get it, but there is no need since string.length() already does that.
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), although it can be
 *        applied to both.
 */
string gpibDevice::read(unsigned long size)
{
    string ret;
    char *tmp_buffer;

    if (debug_flag == true) cout << "read() - 2nd: Enter " << endl;

    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "read() - 2nd: Will read max " << size << " chars from board " << dev_name << endl;
        }
    }
    else
    {
        if (debug_flag == true)
        {
            cout << "read() - 2nd: Will read max " << size << " chars from device " << dev_name << endl;
        }
    }

    resetState();
    tmp_buffer = new char[size+1];
    memset(tmp_buffer,0, (size+1));
    ibrd(dev_handle, tmp_buffer, size);
    saveState();
	
    ret = string(tmp_buffer, dev_ibcnt);
    delete []tmp_buffer;
	
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "read() - 2nd: Error reading from  " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occurs while reading from GPIB ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "read() - 2nd: Leave " << endl;
    return ret;
}


// Remark: In contrast to the above read/write methods where ASCII data
//         were written or read, the two functions below are dedicated for
//         writing and reading BINARY data.

//------------------ gpibDevice::sendData() ----------------

void gpibDevice::sendData(const char *argin, long count)
/**
 * This method performs a write of binary data to a GPIB device
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), since NI-488.2 function
 *        Send() is applicable only to a GPIB device and not a controller.
 */
{
    if (debug_flag == true) cout << "sendData(): Enter " << endl;
    // Check that this function is applied to device object, since only
    // then have device's pad and sad.
    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "sendData(): " << dev_name << \
            " This function is to be applied on device object, not on board"<<endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured when want to execute sendData ", \
            "Intended to do it on Board object.", \
            "Should be done on the Device object", getiberr(), getibsta());
    }

    resetState();
    Send (board_index, MakeAddr(dev_pad, dev_sad), (char *)argin, count, NLend);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
        cout << "sendData(): Error sending binary data to device " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured while writing binary data", \
            iberrToString(), ibstaToString(), getiberr(), getibsta());
    }
    if (debug_flag == true) cout << "sendData(): Leave " << endl;
}


//------------------ gpibDevice::receiveData() ----------------
/**
 * This method performs a read of binary data from a GPIB device
 *
 * Usage: It is not used internally (= neither in this class (gpibDevice)
 *        nor in derived class (gpibBoard).
 *        In device server it is used only on a device object (gpibDevice) 
 *        and not on a controller object (gpibBoard), since NI-488.2 function
 *        Receive() is applicable only to a GPIB device and not a controller.
 */
char *gpibDevice::receiveData(long count)
{
    if (debug_flag == true) cout << "receiveData(): Enter " << endl;

    // Check that this function is applied to device object, since only
    // then have device's pad and sad.
    if (is_a_board == true)
    {
        if (debug_flag == true)
        {
            cout << "receiveData(): " << dev_name << \
            " This function is to be applied on device object, not on board"<<endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured when want to execute receiveData ", \
            "Intended to do it on Board object.", \
            "Should be done on the Device object", getiberr(), getibsta());
    }

    // Allocate buffer for reading data on the GPIB bus
    char* buffer = new char [count];
    if (buffer == NULL)
    {
        if (debug_flag == true)
        {
            cout << "receiveData(): Cannot allocate memory for local buffer for " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Cannot allocate memory for receiveData", \
            iberrToString(), ibstaToString(), getiberr(), getibsta());
    }
    memset(buffer,0, count);
	
    resetState();
    Receive (board_index, MakeAddr(dev_pad, dev_sad), buffer, count, STOPend);
    // The actual number of bytes transferred is returned in the variable
    // ibcntl. We use ThreadIbcntl for thread safety
    saveState();
	
    if (dev_ibsta & ERR)
    {
        delete [] buffer;
        if (debug_flag == true)
        {
            cout << "receiveData(): Error occured in Receive() call on " << dev_name << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occurs while reading binary data from GPIB", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
	
    //long bytes_read=ThreadIbcntl ();
    // CAUTION/TODO : If long bytes_read != count what should we do :
    // a) throw an exception: but it may be too much to take such a decision
    //    at low level
    // b) reallocate a data buffer of the right size (long bytes_read),
    //    copy the buffer data into it, and return this buffer of the rigth
    //    size to the caller. This last opion is our prefered but for now
    //    we prefer to avoid this potential extracopy of data.
	
    if (debug_flag == true) cout << "receiveData(): Leave " << endl;
    return buffer;
}



//
// PROTECTED METHODS of class gpibDevice()
// ---------------------------------------
//

//------------------ gpibDevice::saveState() -------------------
/**
 * This method is for internal class use. 
 * GPIB iberr, ibsta and ibcnt are global to all devices. The gpibDevice 
 * class will call this method after every NI488 / NI488.2 function call, 
 * to save a specific device state. This method must not be used from
 * outside the gpibDevice class.
 *
 * Usage: Since protected, it is used internally in this class (gpibDevice)
 *        and in derived class (gpibBoard).
 */
void gpibDevice::saveState()
{
#ifdef WIN32
    dev_iberr = ThreadIberr ();
    dev_ibsta = ThreadIbsta ();
    dev_ibcnt = ThreadIbcntl ();
#else
    dev_iberr = iberr;
    dev_ibsta = ibsta;
    dev_ibcnt = ibcntl;
#endif
}


//------------------ gpibDevice::resetState() -------------------
/**
 * This method is for internal class use.
 * GPIB iberr, ibsta and ibcnt are global to all devices. The gpibDevice 
 * class will call this method before every NI488 / NI488.2 function call,
 * to reset a specific device state. This method must not be used from
 * outside the gpibDevice class.
 *
 * Usage: Since protected, it is used internally in this class (gpibDevice)
 *        and in derived class (gpibBoard).
 */
void gpibDevice::resetState()
{
    dev_iberr = 0;
    dev_ibsta = 0;
    dev_ibcnt = 0;
}



//+----------------------------------------------------------------------------
//
// 	Class gpibBoard Implementation:
//
//-----------------------------------------------------------------------------

//
// PUBLIC METHODS of class gpibBoard()
// -----------------------------------
//

//------------------ gpibBoard::gpibBoard() 1st -------------------
/**
 * This is the default gpibBoard constructor. It passes the default
 * board_name argument (= "gpib0") to the super-class gpibDevice.
 *
 * Usage: It is not used internally.
 *        In device server not used, since always 2nd constructor is used.
 */
gpibBoard::gpibBoard() :gpibDevice( "gpib0" )
{
    cout << "1st gpibBoard constr: Enter " << endl;
    is_a_board = true;
    board_index = DEFAULT_BOARD_INDEX;
    cout << "1st gpibBoard constr: Leave " << endl;
}


//------------------ gpibBoard::gpibBoard() 2nd -------------------
/**
 * This is a gpibBoard constructor for a specific board. It passes boardname
 * argument in the form gpibN (where N=0,1,2...) to the super-class gpibDevice.
 *
 * Usage: It is not used internally.
 *        In device server used to instantiate a board object when board
 *        is identified by its name.
 */
gpibBoard::gpibBoard(string boardname) :gpibDevice( boardname )
{
    string ss;

    cout << "2nd gpibBoard constr: Enter " << endl;
    is_a_board = true;
    ss = boardname.substr(4);
    board_index = atoi( ss.c_str() );
    cout << "2nd gpibBoard constr: Leave " << endl;
}

// TODO: Maybe provide it as:
//       gpibBoard::~gpibBoard(void) : ~gpibDevice(void)
//       if provide one for gpibDevice class
gpibBoard::~gpibBoard()
{
    // Should use then setOffLine() ??
    cout << "gpibBoard destructor: Enter " << endl;
}

// NI-488.2 system-control methods - related public functions
// ----------------------------------------------------------

//------------------ gpibBoard::sendInterfaceClear() -------------------
/**
 * This method sends an Interface Clear on the bus.
 * All devices are cleared and the board becomes Controler In Charge.
 *
 * Usage: It is not used internally.
 *        In device server used on a board object (gpibBoard), since 
 *        NI-488.2 function SendIFC() is to be applied on a controller board.
 */
void gpibBoard::sendInterfaceClear()
{
    if (debug_flag == true) cout << "sendInterfaceClear(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "sendInterfaceClear(): Will send iterface clear on board " \
             << dev_name << endl;
    }
    resetState();
    SendIFC( board_index );
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "sendInterfaceClear(): Error sending interface clear command on board "<< dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occurred with sendIFC() command", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "sendInterfaceClear(): Leave " << endl;
}


//------------------ gpibBoard::getConnectedDeviceList() ---------
/**
 * Get a list of devices connected on the bus.
 * This method returns a reference on vector of gpibDeviceInfo, containing
 * 3 pieces of information for all GPIB devices found on the GPIB bus.
 * The 3 pieces of info provided in a vector gpibDeviceInfo are:
 * - device's answer to *IDN? request (gives device identification string),
 * - device's primary address and
 * - device's secondary address.
 * Board must be CIC to perform FindLstn !
 *
 * Usage: It is not used internally.
 *        In device server used on a board object (gpibBoard), since 
 *        NI-488.2 function FindLstn() is to be applied on a controller board.
 */
vector<gpibDeviceInfo>& gpibBoard::getConnectedDeviceList(void)
{
    char idn_buffer[MAX_DEVICE_IDENT_STR];
    int loop, nb_listeners;
    // Add 1 for terminal NOADDR = (Addr4882_t)(-1)
    Addr4882_t scanlist[gpib_addr_max+1]; // Array of primary addresses
    Addr4882_t result[gpib_addr_max+1];   // Array of listen addresses

    if (debug_flag == true) cout << "getConnectedDeviceList(): Enter " << endl;

    inf.clear(); // Empty vector first.
    // Build address list to scan, add NOADDR list terminator
    for (loop = 0; loop < gpib_addr_max; loop++) 
    {
        scanlist[loop] = loop+1;
        //result[loop] = 0;
    }
    scanlist[gpib_addr_max] = NOADDR;  // -1 - terminated 

#ifdef COM
    if (debug_flag == true) {
        for (loop = 0; loop < gpib_addr_max; loop++) 
        {
            cout << "getConnectedDeviceList(): scanlist[" << loop << "] = " << scanlist[loop] << endl;
            cout << "getConnectedDeviceList(): result[" << loop << "] = " << result[loop] << endl;
        }
    }
    cout << "getConnectedDeviceList(): scanlist[" << gpib_addr_max << "] = " << scanlist[gpib_addr_max] << endl;
#endif

    if (debug_flag == true)
    {
        cout << endl;
        cout << "getConnectedDeviceList(): Will look for devices connected to "\
             << " board " << dev_name << " with index " << board_index << endl;
        cout << endl;
    }
    resetState();
    FindLstn(board_index, scanlist, result, gpib_addr_max + 1);
    saveState();

#ifdef COM
    // After call to FindLstn()
    cout << "getConnectedDeviceList(): After call to FindLstn()" << endl;
    for (loop = 0; loop < gpib_addr_max; loop++) 
    {
        cout << "getConnectedDeviceList(): result[" << loop << "] = " << result[loop] << endl;
    }
    if (debug_flag == true)
    {
        cout << "getConnectedDeviceList(): iberr = " << iberrToString() << endl;
        cout << "getConnectedDeviceList(): ibsta = " << ibstaToString() << endl;
        cout << "getConnectedDeviceList(): ibcnt = " << dev_ibcnt << endl;
    }
#endif

    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "getConnectedDeviceList(): Error getting devices " << \
                    "connected to board " << dev_name << \
                    " with method FindLstn()" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured with FindLstn on GPIB ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }

    nb_listeners = dev_ibcnt;
    result[nb_listeners] = NOADDR;
    if (debug_flag == true)
    {
        cout << "getConnectedDeviceList(): nb of listeners = " << nb_listeners << endl;
    }

    for (loop = 0; loop < nb_listeners; loop++ )
    {
        gpibDeviceInfo *t = new gpibDeviceInfo();
        t->dev_pad = GetPAD( result[loop] );
        t->dev_sad = GetSAD( result[loop] );
        t->dev_idn = "Device_does_not_support_*IDN?_command.\n";
        resetState();
        Send(board_index, result[loop],(char *)"*IDN?", 5L, NLend);
        saveState();
        if (! (dev_ibsta & ERR) )
        {
            memset(idn_buffer,0, MAX_DEVICE_IDENT_STR);
            Receive(board_index, result[loop], idn_buffer, MAX_DEVICE_IDENT_STR, STOPend);
            // Most of GPIB devices understand '*IDN?' command, and return
            // a string of identification. Some old devices do not implement
            // this command, like Tektronik 2440 which implements its own ID
            // command: 'ID?'. On *IDN? command the 2440 returns char 255,
            // as bad command. Thats why if a device returns an ID string < 5
            // bytes, or finish in Time Out error, we admit that it does not
            // implement command.
            if ( (!(dev_ibsta & ERR)) && (ibcnt > 5) ) // Ibcnt = nb of byte received.
            {
                t->dev_idn = idn_buffer;
            }
        }
        inf.push_back( *t );
    }
    if (debug_flag == true) cout << "getConnectedDeviceList(): Leave " << endl;
    return inf;
}


//------------------ gpibBoard::setActiveController() -------------------
/**
 * This method makes a board to be Active Controller.
 *
 * Usage: In device server used on a board object (gpibBoard), since 
 *        NI-488.1 function ibcac() is to be applied on a controller board.
 */
void gpibBoard::setActiveController(int syncasync)
{
    if (debug_flag == true) cout << "setActiveController(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "setActiveController(): Will make board " << dev_name << " as Active Controller " << endl;
    }
    resetState();
    // If syncasync = 0 ---> board takes control asynchronously = immedidately 
    //			     (= without waiting any data transfer to be 
    //			        completed)
    //              = 1 ---> board takes control synchronously = the board
    //                       waits for any data transfer to be completed
    ibcac(dev_handle, syncasync);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "setActiveController(): Error setting board " << dev_name << " as Active Controller " << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occurred with ibcac() command", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "setActiveController(): Leave " << endl;
}


//------------------ gpibBoard::clearAllDevices() -----------------
/**
 * Clear all connected GPIB devices.
 * This method sends a clear signal on the GPIB bus for all devices 
 * connected to this board.
 * Alternative would be to use in calling environement once all
 * connected devices are obtained with getConnectedDeviceList(),
 * that for each one the calling environement calls getHandle()
 * and then call clearOneDevice(), but using DevClearList() is more
 * practical. 
 *
 * Usage: It is not used internally.
 *        In device server used on a board object (gpibBoard).
 */
void gpibBoard::clearAllDevices(void)
{
    // Add 1 for terminal NOADDR
    Addr4882_t scanlist[gpib_addr_max+1];
    Addr4882_t result[gpib_addr_max+1]; 

    if (debug_flag == true) cout << "clearAllDevices(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "clearAllDevices(): Will clear all devices from the board" \
             << dev_name << endl;
    }

    // get all devices with FindListn()
    // Build address list to scan, add NOADDR list terminator
    for (int loop = 0; loop < gpib_addr_max; loop++)
        scanlist[loop] = (Addr4882_t)(loop+1);
    scanlist[gpib_addr_max] = NOADDR;

    if (debug_flag == true)
    {
        cout << "clearAllDevices(): Will look for devices connected to "\
             << " board " << dev_name << endl;
    }
    resetState();
    FindLstn(board_index, scanlist, result, gpib_addr_max+1);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "clearAllDevices(): Error getting devices " << \
                "connected to board " << dev_name << " with method FindLstn()" \
                 << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured with FindLstn on GPIB ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    result[dev_ibcnt] = NOADDR;

    resetState();
    //DevClearList(board_index, addresslist);
    DevClearList(board_index, result);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "clearAllDevices(): Error in clearing all devices " << \
                    "connected to the board " << dev_name << endl;
        }
        throw gpibDeviceException(dev_name,
           "Error occurs with DevClearList on GPIB ", \
           iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "clearAllDevices(): Leave " << endl;
}


// NI-488.1 methods - related public functions
// -------------------------------------------

//------------------ gpibBoard::sendCommands() -----------------
/**
 * Send a GPIB command message.
 * This method is not used to transmit programming instruction to devices,
 * This kind of instructions are transmitted with the read / write methods.
 * This function sends low-level commands to one or several devices but not
 * with the goal to program/configure this devices for data acquisition/control
 * purposes but to set features like preparing device(s) to be listeners etc.
 *
 * Remark: Alternative to using ibcmd() from NI-488.1 would be to use
 *         SendCmds(board_index, command, count) from NI-488.2, where
 *         command is the command string to be sent and count is the
 *         maximum number of command bytes to be sent.
 *         So with the same argin the call would be:
 *         SendCmds(board_index, (char *) cmd.c_str(), cmd.length());
 *
 * Usage: It is not used internally.
 *        In device server used on a board object (gpibBoard), since 
 *        NI-488.1 function ibcmd is to be applied on a controller board.
 */
int gpibBoard::sendCommands(string cmd)
{
    if (debug_flag == true) cout << "sendCommands(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "sendCommands(): Will send cmd on iterface board " << \
                dev_name << endl;
    }
    resetState();
    ibcmd( dev_handle ,(char *) cmd.c_str(),cmd.length() );
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "sendCommands(): Error sending cmd on board " << \
                    dev_name << " with method ibcmd" << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured with cmd on GPIB ", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "sendCommands(): Leave " << endl;
    return dev_ibcnt;   /* Return saved incnt value */
}


//------------------ gpibBoard::clearOneDevice() -----------------
/**
 * Clear a specified GPIB device.
 * This method sends a clear signal on the GPIB bus for a specified device.
 * Name it clearDev() to distinguish it from clear() in gpibDevice class.
 * Using this function is an alternative to using clear() from gpibDevice class.
 *
 * Usage: It is not used internally.
 *        In device server used on a board object (gpibBoard) and is 
 *        alternative to use clear() on the device object. Of course here
 *        device handle must be passed as argument.
 */
void gpibBoard::clearOneDevice(int devhandle)
{
    if (debug_flag == true) cout << "clearOneDevice(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "clearOneDevice(): Will clear device with handle " << \
        devhandle << " from the board " << dev_name << endl;
    }
    resetState();
    ibclr( devhandle );
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "clearOneDevice(): Error in clearing device with handle " \
                 << devhandle << " from the board " << dev_name << endl;
        }
        throw gpibDeviceException(dev_name,
           "Error occurs with ibclr on GPIB ", \
           iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
    if (debug_flag == true) cout << "clearOneDevice(): Leave " << endl;
}


//------------------ gpibBoard::sendLocalLockout() ----------------------
/**
 * Send a Local Lockout to a specified device.
 * This method send a local lockout to a specified device, prohibiting manual
 * values / setup modifications.
 *
 * Remark1: On NI forum it is said that ibllo() does not work for GPIB-ENET and
 *          that one should rather use ibcmd() with suitable command string!
 * Remark2: Although did not find description of function ibllo() in doc
 *          (in GPIB Library Software Users Manual)
 *          I found it with nm libgpib.a.
 * Remark3: Probably better alternative would be to use SendLLO from NI-488.2
 *          In this case argin to this function would be void, since
 *          SendLLO(board_index) sends local lockout to ALL devices connected
 *          to a controller.
 * TODO: Should we rather use SendLLO?
 *
 * Usage: It is not used internally.
 *        In device server used on a board object (gpibBoard) since we
 *        did not implement a function in gpibDevice() which would call
 *        ibllo(). Since this function is in gpibBoard class, we must provide
 *        device handle as argument.
 */
void gpibBoard::sendLocalLockout(int devhandle)
{
    if (debug_flag == true) cout << "sendLocalLockout(): Enter " << endl;
    if (debug_flag == true)
    {
        cout << "sendLocalLockout(): Will send local lockout to device with handle " << devhandle << " from the board " << dev_name << endl;
    }
    resetState();
    /* Although did not find description of function ibllo() in doc
     * (in GPIB Library Software Users Manual)
     * I found it with nm libgpib.a.
     */
#ifdef _solaris
    ibllo( devhandle );
#endif
#ifdef _linux
    /*
     * Although did not find in doc (in GPIB Library Software Users Manual)
     * found with nm libgpib.a and others (for enet) that ibllo() entry is
     * there.
     * Alternative would be to use NI488.2 function SendLLO(), but be aware
     * that this sends local lockot to ALL devices connected ti a controller.
     *  SendLLO( board_index );
     */
    ibllo( devhandle );
#endif

// alternative would be :
// if defined (_solaris) || defined (_linux)
// ibllo( devhandle )
// endif

// TODO : find ibllo for WIN32

    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "sendLocalLockout(): Error in sending ibllo() to device with handle "<< devhandle << " from the board " << dev_name << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occured with ibllo() on GPIB ", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "sendLocalLockout(): Leave " << endl;
}


//------------------ gpibBoard::setAllToRemoteMode() -------------------
/**
 * This method put ALL GPIB devices connected to one controller into remote
 * mode, since by using NI-488.1 function ibsre() it acts on REN (Remote ENable)
 * line and thus sets NON-selectively ALL connected devices to remote mode.
 *
 * It must be invoked in the gpibBoard instance since the 1st argument
 * to ibsre() is a board handle (and NOT a board index as in original version!!)
 *
 * Remark: Alternative is to use on gpibBoard object the following NI-488.2 
 *         library function:
 *         EnableRemote(board_index, addrlist);
 *         where addrlist is NOADDR-terminated list of all connected devices.
 *         For example if have 2 devices: with primary addr. 6 and 7 would
 *         complose addrlist as follows:
 *         addrlist[0] = 6;
 *         addrlist[1] = 7;
 *         addrlist[2] = NOADDR;
 *         Then EnableRemote() call would set devices with primary addresses
 *         6 and 7 to remote mode.
 *
 * Usage: It is not used internally.
 *        TODO: Currently it is also not used in device server. 
 *              Maybe add a board-level command for this.
 */
void gpibBoard::setAllToRemoteMode(void)
{
    if (debug_flag == true) cout << "setAllToRemoteMode(): Enter " << endl;

    if (debug_flag == true)
    {
        cout << "setAllToRemoteMode(): Will assert REN (Remote Enable) line on board" << dev_name << endl;
    }

    resetState();
    // dev_handle = board handle
    ibsre( dev_handle , 2); // Any non-zero value for second arg will do it
                            // which means all devices controlled by this
                            // board will be in remote mode as soon as they
                            // will be addressed as listeners by the controller
                            // board.
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "setAllToRemoteMode(): Error asserting REN line on board " \
                 << dev_name << " with ibsre() method" << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured with ibsre() command", \
            iberrToString(), ibstaToString(), getiberr(),getibsta() );
    }
    if (debug_flag == true) cout << "setAllToRemoteMode(): Leave " << endl;
}


// Remark: 2 functions here below override those defined in gpibDevice class
//         since the 2 defined in gpibDevice class are not good for a board
//	   (Send and Receive functions from NI488.2 lib are only applicable
//	   to a device, not to a board).

//------------------ gpibBoard::sendData() ----------------

void gpibBoard::sendData(const char *argin, long count)
/**
 * This method performs a write of binary data to a GPIB board 
 *
 * Usage: It is not used internally (= in this class (gpibBoard))
 *        In device server it is used on a board object (gpibBoard) 
 */
{
    if (debug_flag == true) cout << "sendData(): Enter " << endl;

    resetState();
    ibwrt(dev_handle, (char *)argin, count);
    saveState();
    if (dev_ibsta & ERR)
    {
        if (debug_flag == true)
        {
            cout << "sendData(): Error sending binary data to board " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name, \
            "Error occured while writing binary data", \
            iberrToString(), ibstaToString(), getiberr(), getibsta());
    }
    if (debug_flag == true) cout << "sendData(): Leave " << endl;
}


//------------------ gpibBoard::receiveData() ----------------
/**
 * This method performs a read of binary data from a GPIB board 
 *
 * Usage: It is not used internally (= in this class (gpibBoard))
 *        In device server it is used only on a board object (gpibBoard) 
 */
char *gpibBoard::receiveData(long count)
{
    if (debug_flag == true) cout << "receiveData(): Enter " << endl;

    // Allocate buffer for reading data on the GPIB bus
    char* buffer = new char [count];
    if (buffer == NULL)
    {
        if (debug_flag == true)
        {
            cout << "receiveData(): Cannot allocate memory for local buffer for " << dev_name << endl;
        }
        throw gpibDeviceException( dev_name,
            "Cannot allocate memory for receiveData", \
            iberrToString(), ibstaToString(), getiberr(), getibsta());
    }
    memset(buffer,0, count);
	
    resetState();
    ibrd(dev_handle, buffer, count);
    saveState();
	
    if (dev_ibsta & ERR)
    {
        delete [] buffer;
        if (debug_flag == true)
        {
            cout << "receiveData(): Error occured in Receive() call on " << dev_name << endl;
        }
        throw gpibDeviceException(dev_name, \
            "Error occurs while reading binary data from GPIB", \
            iberrToString(), ibstaToString(), getiberr(), getibsta() );
    }
	
    //long bytes_read=ThreadIbcntl ();
    // CAUTION/TODO : If long bytes_read != count what should we do :
    // a) throw an exception: but it may be too much to take such a decision
    //    at low level
    // b) reallocate a data buffer of the right size (long bytes_read),
    //    copy the buffer data into it, and return this buffer of the rigth
    //    size to the caller. This last opion is our prefered but for now
    //    we prefer to avoid this potential extracopy of data.
	
    if (debug_flag == true) cout << "receiveData(): Leave " << endl;
    return buffer;
}

