#ifndef _GPIBDEVICE_H
#define _GPIBDEVICE_H

#include <string>
#include <vector>
#include "gpibAccessException.h"

/**
 * Device's default GPIB board index (i.e. board's logical name = gpib0).
 */
#define DEFAULT_BOARD_INDEX	0

/**
 * Drivers are by default limited to 8 GPIB boards (gpibBoard objects)
 * per driver.
 */
#define MAX_BOARD_INDEX		7 

/**
 * Max number of devices on the GPIB bus (controlled by 1 GPIB board = 1 CIC). 
 * This is used to limit bus scan with getConnectedDeviceList method and 
 * also to check primary address against its possible values on the input 
 * of some functions.
 */
/* #define MAX_DEVICES_PER_BOARD	31 */
/* N.B. With open source gpib driver, which was downloaded to 
 * /segfs/linux/drivers/linux-gpib-3.2.21 we do not need any more 
 * to define such a constant ourselves since there is a constant 
 * gpib_addr_max = 30 defined in gpib_user.h, which is included in ib.h
 * which in turn is included in gpibAccess.cpp.
 */ 

/**
 * Maximum size of string received, when identifying device connected on
 * the GPIB bus (= string return by device in answer to "*IDN?")
 */
#define MAX_DEVICE_IDENT_STR	128

/**
 * Nb of configuration options in ugpib.h or ni488.h file.
 * Each one can be set with ibconfig() or obtained with ibask().
 * The name of those used in ibconfig() starts with Ibc.... and
 * of those used in ibask() starts by Iba....
 * Some options are used only for a device, some only for a board and
 * some are valid both for a device or a board.
 * [Remark: We ignore 15 additional options starting with IbaBNA, 
 *          used only by ibask() and not ibconfig()]
 */
#define NB_OF_CONFIG_OPTS	34

/**
 * Device's default buffer size (in bytes) for read operations.
 */
#define RD_BUFFER_SIZE		512


using namespace std;


/**
 * Next auxiliary function is defined outside the class gpibDevice() so
 * that device server can also use it without having to instantiate a
 * gpibDevice object for ex. when it is looking for the connected devices
 * i.e. GPIB devices connected to a given GPIB-board = CIC = controller in
 * charge.
 * This function is useful for creating a GPIB device name, when it is an
 * empty string like in the last 4 gpibDevice() constructors.
 * This function composes the dev_name in the form "pad_sad", where
 * pad and sad are devices' primary and secondary addresses respectively.
 */
string padAndSadToString(int pad, int sad);


/**
 * This class is designed to handle GPIB devices. Its' point of view is
 * very device oriented: For example, setting device in remote mode,
 * is done with 'goToRemoteMode' device's method. It totaly hides the fact
 * that this is done by sending a command to a GPIB board.
 *
 * This class is designed to work on a SINGLE CIC (Controler In Charge) bus.
 * In simple words this class can't handle multi-board case. The default
 * CIC selected is "gpib0".
 *
 * The usual way to include GPIB feature into a Tango device server consists
 * in single or multiple instance of a gpibDevice / gpibBoard.
 * One simply uses read / write functions to handle devices.
 * Note that ALL operations made on gpibDevice or gpibBoard can potentialy
 * throw a gpibDeviceException.
 */
class   gpibDevice
{

public:

        // 'open'/'close' - related public functions:
        // ------------------------------------------
        // In costructors use ibfind() and ibdev() to 'open' board or device
        // In setOffLine() we use ibonl() to 'close' board or device

        // 6 class constructors in which 'open' is done to a GPIB device
        // only the 2nd constructor is also used to 'open' a GPIB board
        gpibDevice(string devname, string boardname);
        gpibDevice(string boarddev);
        gpibDevice(int pad, string boardname);
        gpibDevice(int pad);
        gpibDevice(int pad, int sad, string boardname);
        gpibDevice(int pad, int sad);
        //~gpibDevice(void);              // Classs destructor.

        void setOffLine(void);          // 'close', set offline GPIB device
                                        // or GPIB board (when in derived class)
                                        // taken with ibfind or ibdev.
                                        // = free device or board handle.


        // debug - related public functions:
        // ---------------------------------
        void setDebug(bool dbg);        // set value for debug flag
        bool getDebug(void);            // get value of  debug flag


        // status and error - related public functions:
        // --------------------------------------------
        string ibstaToString(void);     // Get string from ibsta string.
        string iberrToString(void);     // Get string from iberr string.
        int getiberr(void);             // Get device iberr value.
        int getibsta(void);             // Get device ibsta value.
        unsigned int getibcnt(void);    // Get device ibcnt value.


        // some informative public functions
        // ---------------------------------
        string getName(void);           // Return device name as given on
                                        // input to 1st or 2nd constructor or
                                        // constructed from PAD+SAD for other
                                        // constructors. When used on board
                                        // (derived class) object it returns
                                        // board name.
        int getHandle(void);            // Get device/board handle.
        int getPad(void);               // Get device/board GPIB primary addr.;
        int getSad(void);               // Get device/board GPIB secondary addr;
        int getBoardIndex(void);        // Get index of GPIB board

        // configuration - related public functions
        // ----------------------------------------
        void setTimeOut(int tmo);       // Set device/board time out.
        int getTimeOut(void);           // Get device/board time out.
        void setConfig(int opt,int v);  // Send ibconfig request to
                                        // device/board (= set config)
        int getConfig(int opt);         // Send ibask request to
                                        // device/baard (= get config)

        // device control - related public functions
        // -----------------------------------------
        short getSerialPollByte(void);  // Serial polls a device
        void clear(void);               // Clear the GPIB device.
        void trigger(void);             // Sends trigger signal to GPIB device.
        short isAlive(void);            // Check the presence of the device
                                        // on the bus.

        // device/board control - related public functions
        // -----------------------------------------------
        void goToLocalMode(void);       // set device/board to local mode

        // bus control - related public functions
        // --------------------------------------
        void goToRemoteMode(void);      // set device into remote mode.


        // IO - related public functions
        // -----------------------------
        int write(string);              // Send a string to a GPIB device or
                                        // board.
        string read(void);              // Read a string of max 512 chars
                                        // from a GPIB device or board.
        string writeRead(string);       // Perform a write/read operation
                                        // on a device or board in a row(query).
        string read(unsigned long size);// Read a string of max 'size' chars
                                        // from a GPIB device or board.


        void sendData(const char *, long count); // Write Binary data on
                                                 // a GPIB device
        char* receiveData(long count);  // Read Binary data from a GPIB device

protected:

        void saveState(void);           // save iberr/ibstat/ibcntl in
                                        // dev_ibsta/dev_iberr/dev_ibcnt.
        void resetState(void);          // reset dev_ibsta/dev_iberr/dev_ibcnt

        bool is_a_board;                // flag which indicates whether the
                                        // instance is device or board.
        /**
         * Internal GPIB handle returned by:
         * - ibfind() when using logical name of device or board or
         * - ibdev(), when using address of device controlled by given board.
         */
        int dev_handle;

        /**
         * GPIB Primary Address returned by ibask(devHandle,IbaPAD,...).
         */
        int dev_pad;

        /**
         * GPIB Secondary Address returned by ibask(devHandle,IbaSAD,...).
         */
        int dev_sad;

        /**
         * Internal GPIB ibsta copy.
         */
        int dev_ibsta;

        /**
         * Internal GPIB iberr copy.
         */
        int dev_iberr;

        /**
         * Internal GPIB ibcntl copy.
         */
        unsigned int dev_ibcnt;

        /**
         * Internal read operation buffer.
         */
        char rd_buffer[RD_BUFFER_SIZE+1];

        /**
         * GPIB device name passed on input to 1st or 2nd constructor,
         * or built from primary and secondary addresses in other
         * 4 constructors.
         * In 2nd constructor boardname is copied into this variable,
         * so it contains in this case the board name.
         */
        string dev_name;

        /**
         * This is the GPIB board index, which in case of dealing
         * with device is the board to which a device is connected to.
         * Remark: originally it was in private: block, but is better
         *         to have it here, so derived class gpibBoard() can
         *         use it and does not need any more it's private copy
         *         board_id
         */
        int    board_index;

        /**
         * Check that device is present on the bus.
         */
        short alive;

        /**
         * Debug flag for methods
         * Remark: error messages are always printed independent of the
         *         value of this flag.
         */
        bool debug_flag;

};


/**
 * This class is a container for Device Info.
 * No methods are implemented, it only features public fields. This class
 * is mainly used with getConnectedDeviceList method.
 */

class gpibDeviceInfo {

public:
        /**
         * This string contains device's answer to "*IDN?"
         */
        string	dev_idn;

        /**
         * This int contains device's Primary ADdress.
         */
        int	dev_pad;
	
        /**
         * This int contains device's Secondary ADdress.
         */
        int	dev_sad;
};


/**
 * This class is designed to handle gpibBoards. gpidBoard can be
 * seen as gpibDevice with more features. That's why this class inherits from
 * gpibDevice and adds methods to handle board features. 
 */
class gpibBoard : public gpibDevice
{

public:

        // 'open'/'close' - related public functions:
        // ------------------------------------------
        gpibBoard(void);                // Class default constructor for gpib0.
        gpibBoard(string boardname);    // Constructor for specified board.
        ~gpibBoard(void);               // Classs destructor.

        // NI-488.2 system-control methods - related public functions
        // ----------------------------------------------------------
        void sendInterfaceClear(void);	// Send GPIB Interface Clear
        vector<gpibDeviceInfo>& getConnectedDeviceList();
        void setActiveController(int syncasync);// Make Board the Active 
                                                // Controller
        void clearAllDevices(void);     // Clear all devices connected to a board.
	
        // NI-488.1 methods - related public functions
        // -------------------------------------------
        int sendCommands(string);       // Send GPIB command message
        void clearOneDevice(int devhandle);// Clear specified device.
        void sendLocalLockout(int devhandle); // Send Local Lockout to a device.
        void setAllToRemoteMode(void);  // Sets All connected devices to remote
                                        // mode by asserting REN line.

        // next 2 override the ones defined in gpibDevice class
        void sendData(const char *, long count); // Write Binary data on
                                                 // a GPIB board
        char* receiveData(long count);  // Read Binary data from a GPIB board 

private:
        vector<gpibDeviceInfo> inf;
};

#endif
